package cz.cvut.fel.ts1;

public class Factorial {
	public static long factorial(final int n) {
		int acc = 1;

		for (int i = 1; i <= n; i++) {
			acc *= i;
		}

		return acc;
	}
}
